import { TextDifferent } from './text-different.model';
import LineModification from './line-modification.abstract';
import { ELineModifications } from './line-modifications.enum';

export class LineContentModificationDTO extends LineModification {
  public readonly changes: TextDifferent[];
  public readonly type = ELineModifications.contentModification;

  public constructor(dto: Partial<LineContentModificationDTO>) {
    super(dto);

    if (!dto.changes || dto.changes.length === 0) throw new Error(''); // TODO: set error message

    this.changes = dto.changes;
  }

  public clone(): LineContentModificationDTO {
    return new LineContentModificationDTO({
      index: this.index,
      changes: JSON.parse(JSON.stringify(this.changes)) as TextDifferent[], // deep copy
    });
  }
}
