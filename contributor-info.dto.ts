export default class ContributorInfoDTO {
  public username: string;
  public email: string;

  public constructor(model: Partial<ContributorInfoDTO>) {
    if (!model.username) throw new Error('Username is invalid!');
    if (!model.email) throw new Error('Email is invalid!');

    this.username = model.username;
    this.email = model.email;
  }
}
