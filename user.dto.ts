import DocumentDTO from './document.dto';

export default class UserDTO {
  /**  */
  public username: string;

  public documents: DocumentDTO[];

  public collaboDocs: DocumentDTO[]; // Collaboration documents

  public constructor(model: Partial<UserDTO>) {
    if (!model.username) throw new Error('Invalid username!');
    if (!model.documents || !Array.isArray(model.documents))
      throw new Error('Invalid documents!');

    this.username = model.username;
    this.documents = model.documents;
    this.collaboDocs = model.collaboDocs ?? [];
  }
}
