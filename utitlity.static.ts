import { TextDifferent } from './text-different.model';
import { diff_match_patch as Diff } from 'diff-match-patch';
import { EChangeType } from './change-type.enum';
import { LineInsertionDTO } from './line-insertion.dto';
import { LineDeletionDTO } from './line-deletion.dto';
import LineModification from './line-modification.abstract';
import { LineContentModificationDTO } from './line-content-modification.dto';
import { ELineModifications } from './line-modifications.enum';
import { DOCUMENT_NAME_MIN_LENGTH } from './constants';
import { LineRevertDTO } from './line-revert.dto';

export function isDocumentNameValid(docName: string): boolean {
  return docName.length >= DOCUMENT_NAME_MIN_LENGTH;
}

export function getDifferent(textA: string, textB: string): TextDifferent[] {
  const result: TextDifferent[] = [];

  for (const diff of new Diff().diff_main(textA, textB)) {
    if (diff[0] === EChangeType.equality) {
      result.push(diff[1].length);
    } else {
      result.push(diff);
    }
  }

  return result;
}

export function mergeText(text: string, changes: TextDifferent[]): string {
  let currentIndex = 0;
  for (const change of changes) {
    if (Array.isArray(change)) {
      const changeType: EChangeType = change[0];
      if (changeType === EChangeType.deletion) {
        const newText = text.split('');
        newText.splice(currentIndex, change[1].length);

        text = newText.join('');
      } else if (changeType === EChangeType.insertion) {
        const newText = text.split('');
        newText.splice(currentIndex, 0, change[1]);

        text = newText.join('');

        currentIndex += change[1].length;
      }
    } else {
      currentIndex += change;
    }
  }

  return text;
}

export function mergeLines(
  lines: string[],
  changes: LineModification[],
): string[] {
  const result: string[] = [...lines];
  if (result.length === 0) result.push('');

  for (const change of changes) {
    if (change instanceof LineInsertionDTO) {
      result.splice(change.index, 0, '');
    } else if (change instanceof LineDeletionDTO) {
      result.splice(change.index, 1);
    } else if (change instanceof LineContentModificationDTO) {
      if (change.index >= result.length) throw new Error('');

      result[change.index] = mergeText(result[change.index], change.changes);
    }
  }

  return result;
}

export function convertJSONClassesToRealClasses(
  jsonClasses: LineModification[],
): LineModification[] {
  return jsonClasses.map((c) => {
    if (c.type === ELineModifications.insertion)
      return new LineInsertionDTO(c as LineInsertionDTO);
    else if (c.type === ELineModifications.deletion)
      return new LineDeletionDTO(c as LineDeletionDTO);
    else if (c.type === ELineModifications.contentModification)
      return new LineContentModificationDTO(c as LineContentModificationDTO);
    else if (c.type === ELineModifications.revert)
      return new LineRevertDTO(c as LineRevertDTO);

    throw new Error(''); // TODO: set message
  });
}

export function sumUpTextDiffs(diffs: TextDifferent[]): TextDifferent[] {
  // convert all empty insertions and deletions in a number
  for (let i = 0; i < diffs.length; ++i) {
    const currentDiff = diffs[i];
    if (Array.isArray(currentDiff) && currentDiff[1].length === 0) {
      diffs[i] = 0;
    }
  }

  // sum up all numbers in row to one number
  let lastOneWasNumber = false;
  for (let i = 0; i < diffs.length; ++i) {
    const currentDiff = diffs[i];
    const isEqualityNumber = !Array.isArray(currentDiff);

    // check if it is normal number
    if (isEqualityNumber) {
      // the last one was a number, so sum up!
      if (lastOneWasNumber) {
        diffs[i - 1] = (diffs[i - 1] as number) + (currentDiff as number);

        // delete number in array
        diffs.splice(i, 1);
        --i; // decrease because now i contains the next element
      }
    }

    lastOneWasNumber = isEqualityNumber;
  }

  // check if first number is 0
  if (diffs.length > 0 && diffs[0] === 0) {
    diffs.splice(0, 1);
  }

  // check if last number is 0
  if (diffs.length > 0 && diffs[diffs.length - 1] === 0) {
    diffs.splice(diffs.length - 1, 1);
  }

  return diffs;
}

export function sumUpLineModifications(
  lines: LineModification[],
): LineModification[] {
  for (let i = 0; i < lines.length; ++i) {
    const line = lines[i];
    if (line instanceof LineContentModificationDTO) {
      lines[i] = new LineContentModificationDTO({
        index: line.index,
        changes: sumUpTextDiffs(line.changes),
      });
    }
  }
  return lines;
}
