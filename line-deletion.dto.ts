import LineModification from './line-modification.abstract';
import { ELineModifications } from './line-modifications.enum';

export class LineDeletionDTO extends LineModification {
  public readonly type = ELineModifications.deletion;

  public constructor(dto: Partial<LineDeletionDTO>) {
    super(dto);
  }

  public clone(): LineDeletionDTO {
    return new LineDeletionDTO({ index: this.index });
  }
}
