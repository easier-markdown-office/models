import UserDto from './user.dto';

/**
 * This class represents data transfer objects that are supposed to deliver information back to the user
 * that has been formed as a result of a successful login.
 */
export default class LoginResultDTO extends UserDto {
  /** This variable represents the token that has been associated with the login of the user. */
  public token: string;

  /**
   * This is the constructor of the DTO.
   * @param model This model represents an object that is supposed to contain the information the actual DTO should hold.
   */
  public constructor(model: Partial<LoginResultDTO>) {
    super(model);
    if (!model.token) throw new Error('Invalid token!');

    this.token = model.token;
  }
}
