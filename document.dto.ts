export default class DocumentDTO {
  public name: string;

  public id: string;

  public modDate: number;

  public constructor(model: Partial<DocumentDTO>) {
    if (!model.name) throw new Error('The name is required!');
    if (!model.id) throw new Error('The id is required!');

    this.id = model.id;
    this.name = model.name;
    this.modDate = model.modDate ?? 0;
  }
}
