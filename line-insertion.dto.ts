import LineModification from './line-modification.abstract';
import { ELineModifications } from './line-modifications.enum';

export class LineInsertionDTO extends LineModification {
  // public readonly lineContent?: string; // too complex to implement, just insert a new line and then set the text...
  public readonly type = ELineModifications.insertion;

  public constructor(dto: Partial<LineInsertionDTO>) {
    super(dto);
  }

  public clone(): LineInsertionDTO {
    return new LineInsertionDTO({
      index: this.index,
    });
  }
}
