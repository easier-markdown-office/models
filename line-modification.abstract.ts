import { ELineModifications } from './line-modifications.enum';

export default abstract class LineModification {
  public /* readonly */ index: number;
  public abstract readonly type: ELineModifications;

  protected constructor(dto: Partial<LineModification>) {
    if (dto.index === undefined || dto.index < 0) throw new Error(''); // TODO: set error message

    this.index = dto.index;
  }

  public abstract clone(): LineModification;
}
