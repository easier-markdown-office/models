import UpdateCursorDTO from './update-cursor.dto';

export type CursorContributorDTO = UpdateCursorDTO & {
  id: string;
};
