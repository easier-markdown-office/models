/**
 *
 * See https://github.com/google/diff-match-patch/wiki/API
 */
export enum EChangeType {
  deletion = -1,
  equality = 0,
  insertion = 1,
}
