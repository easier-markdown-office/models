export enum ELineModifications {
  contentModification = 'contentMod',
  deletion = 'deletion',
  insertion = 'insertion',
  revert = 'revert',
}
