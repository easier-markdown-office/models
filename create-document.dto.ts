export default class CreateDocumentDTO {
  public name: string;

  public text?: string;

  public constructor(model: Partial<CreateDocumentDTO>) {
    if (!model.name) throw new Error('The name is required!');

    this.name = model.name;
    this.text = model.text;
  }
}
