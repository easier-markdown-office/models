export default class WsInvitationDTO {
  public id: string;
  public email: string;

  public constructor(model: Partial<WsInvitationDTO>) {
    if (!model.id) throw new Error('Invalid ID!');
    if (!model.email) throw new Error('Invalid e-mail address!');

    this.id = model.id;
    this.email = model.email;
  }
}
