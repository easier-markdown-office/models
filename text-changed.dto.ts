import LineModification from './line-modification.abstract';

export type TextChangedDTO = {
  changes: LineModification[];
  hash: number;
  checksum: string; // TODO: implement
};
