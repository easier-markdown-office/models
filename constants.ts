export const USERNAME_MIN_LENGTH = 2;

export const PASSWORD_MIN_LENGTH = 5;

export const DOCUMENT_NAME_MIN_LENGTH = 2;
