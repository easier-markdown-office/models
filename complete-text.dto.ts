export type CompleteTextDTO = {
  lines: string[];
  hash: number;
};
