import LineModification from './line-modification.abstract';

export type AdjustClientTextDTO = {
  missedMods: LineModification[][];
  hash: number;
  checksum: string; // TODO: implement
};
