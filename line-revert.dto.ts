import LineModification from './line-modification.abstract';
import { ELineModifications } from './line-modifications.enum';

export class LineRevertDTO extends LineModification {
  public readonly type = ELineModifications.revert;

  public constructor(dto: Partial<LineRevertDTO>) {
    super(dto);
  }

  public clone(): LineRevertDTO {
    return new LineRevertDTO({
      index: this.index,
    });
  }
}
