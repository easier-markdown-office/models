export default class RegisterDTO {
  public username: string;
  public email: string;
  public password: string;

  public constructor(model: Partial<RegisterDTO>) {
    if (!model.username) throw new Error('Invalid username!');
    if (!model.email) throw new Error('Invalid email!');
    if (!model.password) throw new Error('Invalid password!');

    this.username = model.username;
    this.email = model.email;
    this.password = model.password;
  }
}
