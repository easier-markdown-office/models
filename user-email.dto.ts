export default class UserEmailDTO {
  public email: string;

  public constructor(model: Partial<UserEmailDTO>) {
    if (!model.email || !model.email.includes('@'))
      throw new Error('Invalid email address'); // it is not a real validation but it will check by the backend

    this.email = model.email;
  }
}
