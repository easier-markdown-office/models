export default class UpdateCursorDTO {
  public start: number;
  public end: number;

  public constructor(model: Partial<UpdateCursorDTO>) {
    if (
      model.start === null ||
      model.start === undefined ||
      isNaN(parseInt(model.start as any))
    )
      throw new Error('Invalid start!');

    if (
      model.end === null ||
      model.end === undefined ||
      isNaN(parseInt(model.end as any))
    )
      throw new Error('Invalid end!');

    this.start = model.start;
    this.end = model.end;
  }
}
