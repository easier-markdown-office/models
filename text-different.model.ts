import { Diff } from 'diff-match-patch';

export type TextDifferent = Diff | number;
