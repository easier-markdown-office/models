export const DOCUMENT_GATEWAY_PREFIX = 'document';
export const DOCUMENT_EVENTS = {
  textChanged: `${DOCUMENT_GATEWAY_PREFIX}/text/changed`,
  created: `${DOCUMENT_GATEWAY_PREFIX}/created`,
  removed: `${DOCUMENT_GATEWAY_PREFIX}/removed`,
  renamed: `${DOCUMENT_GATEWAY_PREFIX}/renamed`,
  modDateChanged: `${DOCUMENT_GATEWAY_PREFIX}/modificationDateChanged`,
  collaboCreated: `${DOCUMENT_GATEWAY_PREFIX}/collabo/created`,
  collaboRemoved: `${DOCUMENT_GATEWAY_PREFIX}/collabo/removed`,
  collaboRenamed: `${DOCUMENT_GATEWAY_PREFIX}/collabo/renamed`,
  collaboModDateChanged: `${DOCUMENT_GATEWAY_PREFIX}/collabo/modificationDateChanged`,
};
export const DOCUMENT_MESSAGES = {
  completeText: `${DOCUMENT_GATEWAY_PREFIX}/text`,
  completeTextWithoutSideEffects: `${DOCUMENT_GATEWAY_PREFIX}/text/withoutSideEffects`,
  changeText: `${DOCUMENT_GATEWAY_PREFIX}/text/change`,
  create: `${DOCUMENT_GATEWAY_PREFIX}/create`,
  delete: `${DOCUMENT_GATEWAY_PREFIX}/delete`,
  rename: `${DOCUMENT_GATEWAY_PREFIX}/rename`,
  invite: `${DOCUMENT_GATEWAY_PREFIX}/invite`,
  discharge: `${DOCUMENT_GATEWAY_PREFIX}/discharge`,
  leave: `${DOCUMENT_GATEWAY_PREFIX}/leave`,
};

export const CURSOR_GATEWAY_PREFIX = 'cursor';
export const CURSOR_EVENTS = {
  changed: `${CURSOR_GATEWAY_PREFIX}/contri/cursor`,
  cursors: `${CURSOR_GATEWAY_PREFIX}/contri/cursors`,
  contriAdded: `${CURSOR_GATEWAY_PREFIX}/contri/joined`,
  contriRemoved: `${CURSOR_GATEWAY_PREFIX}/contri/left`,
};
export const CURSOR_MESSAGES = {
  change: `${CURSOR_GATEWAY_PREFIX}/cursor`,
};

export const USER_GATEWAY_PREFIX = 'user';
export const USER_EVENTS = {
  counter: `${USER_GATEWAY_PREFIX}`,
  // userID: `${USER_GATEWAY_PREFIX}/id`,
};
/*
export const USER_MESSAGES = {
  checkIn: `${USER_GATEWAY_PREFIX}/checkIn`,
  // unstableTestID: `${USER_GATEWAY_PREFIX}/unstable/id`,
};
 */

export const ERROR_EVENTS = {
  merging: 'error_merging',
};
