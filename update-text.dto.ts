import LineModification from './line-modification.abstract';

export default class UpdateTextDTO {
  public changes: LineModification[];
  public basedOn: number;

  public constructor(model: Partial<UpdateTextDTO>) {
    if (!model.basedOn) throw new Error('Invalid basedOn ID!');
    if (!model.changes || !Array.isArray(model.changes))
      throw new Error('Invalid changes!');

    this.basedOn = model.basedOn;
    this.changes = model.changes;
  }
}
