/**
 * Represents a simple data object to transfer the username/email address and the password of a user.
 * This class is mainly used to send the login information as content of the header, so that the content is encrypted
 * (when SSL is active).
 */
export default class LoginDataDTO {
  /** Represents the username OR the email address of the user. */
  public user: string;

  /** Represents the password of the user (this specific value is being hashed prior to persisting it in the database). */
  public password: string;

  /**
   * This is the constructor of the DTO.
   * @param model This model represents an object that is supposed to contain the information the actual DTO should hold.
   */
  public constructor(model: Partial<LoginDataDTO>) {
    if (!model.user)
      throw new Error('The user data (username or email address) is invalid!');
    if (!model.password) throw new Error('The password is required!');

    this.user = model.user;
    this.password = model.password;
  }
}
